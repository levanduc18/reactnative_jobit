/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { QuickView, Text, Container, Header, Body, Image, Button } from '@components';
import { StatusBar } from 'react-native';
import { Global } from '@utils/appHelper';
import NavigationService from '@utils/navigation';
import rootStack from '@contents/routes';
import MyJobTopTab from '../containers/index.toptab';

class MyJobScreen extends PureComponent {
  render() {
    const { token } = Global;
    return token ?
      (
        <Container>
          <StatusBar backgroundColor="transparent" />
          <Header
            backIcon
            leftColor="#fff"
            backgroundColor="#5864ec"
            height={80}
            title="My Job"
          />
          <MyJobTopTab />
        </Container>
      ) :
      (
        <Container>
          <Header
            backIcon
            leftColor="#fff"
            backgroundColor="#5864ec"
            height={100}
            title="My Job"
          />
          <QuickView justifyContent="center" alignItems="center" flex={1}>
            <Text fontSize={20}>You are not logged in now.</Text>
            <Button onPress={() => { NavigationService.navigate(rootStack.authStack); }} title="Login or Register"></Button>
          </QuickView>
        </Container>
      )
  }
}

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(MyJobScreen);
