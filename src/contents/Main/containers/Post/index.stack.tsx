import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import profileStack from '../Profile/routes';
import ProfileScreen from './screens';

const Stack = createStackNavigator();
export default function ProfileStack() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name={profileStack.index} component={ProfileScreen} />
    </Stack.Navigator>
  );
}
