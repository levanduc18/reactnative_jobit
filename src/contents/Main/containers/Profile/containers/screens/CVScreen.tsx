import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  Body, Button, Container, Header, QuickView, Text,
} from '@components';
import { applyObjectSelector, parseObjectSelector } from '@utils/selector';
import { profileSelector } from '@contents/Main/containers/Profile/redux/selector';
import axios from 'axios';
import {
  Linking, StatusBar, TouchableOpacity, Alert, StyleSheet, ScrollView, Dimensions,
} from 'react-native';
import { profileGetDetail } from '@contents/Main/containers/Profile/redux/slice';
import { TQuery } from '@utils/redux';
import { FlatList, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { Icon, Slider, Divider } from 'react-native-elements';
import DocumentPicker from 'react-native-document-picker';

const styles = StyleSheet.create({
  listItem: {

    borderRadius: 9,
    zIndex: -3,
    backgroundColor: '#ffffff',
    paddingTop: 15,
    paddingBottom: 20,
    paddingHorizontal: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,
    elevation: 11,
    marginBottom: 15,
    flexGrow: 0,
  },
});

interface Props {
  getDetailProfile: any;
  profile: any;
}

interface State {
  listCV: any
  isLoading: boolean
}
class CVScreen extends PureComponent<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = { listCV: [], isLoading: true };
  }

  componentDidMount() {
    const { getDetailProfile } = this.props;
    getDetailProfile();
  }

  renderCenterComponent = () => (
    <Text color="#fff" fontSize={20} fontWeight="bold">
      Manage CV
    </Text>
  );

  renderRightComponent = () => (
    <Icon
      name="plus"
      type="antdesign"
      color="white"
      onPress={async () => {
        try {
          const {
            profile: { data },
          } = this.props;
          const uploadFile = await DocumentPicker.pick({
            type: [
              DocumentPicker.types.pdf,
              DocumentPicker.types.docx,
              DocumentPicker.types.zip,
            ],
          });
          const image: any = {
            uri: uploadFile.uri,
            type: uploadFile.type,
            name: uploadFile.name,
          };
          const file = new FormData();
          file.append('file', image);
          const result: any = await axios.post(
            'https://7dc66fda7ef5.ap.ngrok.io/upload',
            file,
            {
              headers: {
                'Content-Type': 'multipart/form-data; ',
              },
            },
          );

          await axios.post(
            `https://7dc66fda7ef5.ap.ngrok.io/apply/cv/${data.id}`,
            {
              name: image.name,
              cvURL: result.data.data.url,
            },
          );
          if (result.data.data.url) {
            this.setState({ isLoading: !this.state.isLoading });
            Alert.alert('Add successful!');
          }
        } catch (err) {
          // eslint-disable-next-line no-console
          console.log('err', err);
        }
      }}
    />
  );

  renderListCV = ({ item }: { item: any }) => (
    <QuickView>
      <QuickView style={styles.listItem}>
        <QuickView marginTop={10}>
          <Text
            color="#1D1D1D"
            fontSize={16}
            fontWeight="bold"
            style={{ letterSpacing: 0.5 }}
            fontFamily="GothamRoundedBold"
          >
            File name:
          </Text>
          <Text
            color="#1D1D1D"
            fontSize={16}
          >
            {item.name}
          </Text>
        </QuickView>
        <QuickView marginTop={10}>
          <Text
            color="#1D1D1D"
            fontSize={16}
            fontWeight="bold"
            style={{ letterSpacing: 0.5 }}
            fontFamily="GothamRoundedBold"
          >
            Link :
            {' '}
          </Text>
          <Text
            color="#1D1D1D"
            fontSize={16}
          >
            {item.cvURL}
          </Text>
        </QuickView>
        <Divider style={{ backgroundColor: '#000', marginTop: 10 }} />
        <QuickView row justifyContent="space-around" marginTop={15}>
          <Icon
            name="eye"
            color="rgba(173, 181, 189, 0.7)"
            type="antdesign"
              // style={{ marginRight: 20, paddingRight:20 }}
            onPress={() => { Linking.openURL(item.cvURL); }}
          />
          <Icon
              // style={{ marginLeft: 20, justifyContent: 'center' }}
            name="delete"
            color="rgba(173, 181, 189, 0.7)"
            type="antdesign"

            onPress={async () => {
              await axios.delete(
                `https://7dc66fda7ef5.ap.ngrok.io/apply/cv/${item.id}`,
              );
            }}
          />
        </QuickView>
      </QuickView>
    </QuickView>
  );

  render() {
    const {
      profile: { data },
    } = this.props;
    const self = this;
    axios.get(
      `https://7dc66fda7ef5.ap.ngrok.io/apply/cv/${data.id}`,
    ).then((response) => {
      self.setState({ listCV: response.data.data });
    });
    if (Object.keys(this.state.listCV).length > 0) {
      return (
        <Container>
          <StatusBar backgroundColor="transparent" />
          <Header
            backIcon
            leftColor="#fff"
            backgroundColor="#5760eb"
            height={100}
            centerComponent={this.renderCenterComponent()}
            rightComponent={this.renderRightComponent()}
          />
          <QuickView alignItems="center" flex={1}>
            <FlatList
              data={this.state.listCV}
              renderItem={this.renderListCV}
              contentContainerStyle={{
                flexDirection: 'row',
                flexWrap: 'wrap',
              }}
            />
          </QuickView>
        </Container>
      );
    }
    return (
      <Container>
        <StatusBar backgroundColor="transparent" />
        <Header
          backIcon
          leftColor="#fff"
          backgroundColor="#5760eb"
          height={100}
          centerComponent={this.renderCenterComponent()}
          rightComponent={this.renderRightComponent()}
        />

        {/* <QuickView row justifyContent="space-between"> */}
        <QuickView row alignItems="center">
          <Text>No CV in your profile, please click + button to add one</Text>
        </QuickView>
        {/* </QuickView> */}

      </Container>
    );
  }
}

const mapStateToProps = (state: any) => ({
  profile: parseObjectSelector(applyObjectSelector(profileSelector, state)),
});

const mapDispatchToProps = (dispatch: any) => ({
  getDetailProfile: (query?: TQuery) => dispatch(profileGetDetail({ query })),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CVScreen as any);
