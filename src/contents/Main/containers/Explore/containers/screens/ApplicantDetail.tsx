/* eslint-disable react/no-unescaped-entities */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  QuickView,
  Text,
  Container,
  Header,
  Body,
  ParallaxScrollView,
  Image,
  Button,
} from '@components';
import { Icon, withTheme, Overlay } from 'react-native-elements';
import { TouchableOpacity, Alert } from 'react-native';
import { getIdFromParams, Global } from '@utils/appHelper';
import { applyObjectSelector, parseObjectSelector } from '@utils/selector';
import { loginSelector } from '@contents/Auth/containers/Index/Login/redux/selector';
import { requireLoginSelector } from '@contents/Config/redux/selector';
import NavigationService from '@utils/navigation';
import rootStack from '@contents/routes';
import { myJobsGetRecently } from '@contents/Main/containers/MyJobs/redux/slice';
import axios from 'axios';
import { profileGetDetail } from '@contents/Main/containers/Profile/redux/slice';
import { TQuery } from '@utils/redux';
import { profileSelector } from '@contents/Main/containers/Profile/redux/selector';
import TopTabs from './TopTabs';
import { jobApplies, jobGetDetail } from '../../redux/slice';
import { jobDetailSelector } from '../../redux/selector';
import exploreStack from '../../routes';

interface Props {
  getDetail: (id: string) => any;
  detail: any;
  loginSelectorData?: any;
  appliesJob: (id: string) => any;
  getDetailProfile: any;
  data: any;
  getList: any;
  profile: any;
}

interface State {
  isVisible: boolean;
  errMsg: String;
  isApplied:boolean;
}
class ApplicantScreens extends PureComponent<Props, State> {
  DateDiff = {
    inHours(d1: number, d2: number) {
      return Math.floor((d2 - d1) / (3600 * 1000));
    },
    inMinutes(d1: number, d2: number) {
      return Math.floor((d2 - d1) / (60 * 1000));
    },
    inDays(d1: number, d2: number) {
      return Math.floor((d2 - d1) / (24 * 3600 * 1000));
    },
    inMonth(d1: number, d2: number) {
      return Math.floor((d2 - d1) / (30 * 24 * 3600 * 1000));
    },
  };

  constructor(props: any) {
    super(props);
    this.state = {
      isVisible: true,
      errMsg: '',
      isApplied: false,
    };
  }

  componentDidMount() {
    const { getDetail, getList, getDetailProfile } = this.props;
    getDetailProfile();
    getList();
    getDetail(getIdFromParams(this.props));
  }

  componentDidUpdate() {
    const { getDetail, getList, getDetailProfile } = this.props;
    getDetailProfile();
    getList();
    getDetail(getIdFromParams(this.props));
  }

  render() {
    const Id = this.props.profile.data.id;
    const { isApplied, errMsg } = this.state;
    const { token } = Global;
    const self = this;
    axios.get(
      `https://7dc66fda7ef5.ap.ngrok.io/api/v1/jobs/detail/${getIdFromParams(this.props)}?userId=${Id}`,
    ).then((response) => {
      // self.setState({ hi: response.data.data });
      console.log(response.data.data.isApplied);
      self.setState({ isApplied: response.data.data.isApplied });
    });

    let datediff;
    const {
      detail: { data, loading },
    } = this.props;
    // eslint-disable-next-line react/destructuring-assignment
    // const { data: applied } = this.props.detail.data;
    // if (applied) this.setState({ isApplied: true });
    // else this.setState({ isApplied: false });
    if (
      this.DateDiff.inMinutes(
        new Date(data.createdat).getTime(),
        new Date().getTime(),
      ) < 60
    ) {
      datediff = `${this.DateDiff.inMinutes(
        new Date(data.createdat).getTime(),
        new Date().getTime(),
      )} minutes ago`;
    } else if (
      this.DateDiff.inHours(
        new Date(data.createdat).getTime(),
        new Date().getTime(),
      ) < 24
    ) {
      datediff = `${this.DateDiff.inHours(
        new Date(data.createdat).getTime(),
        new Date().getTime(),
      )} hours ago`;
    } else {
      datediff = `${this.DateDiff.inDays(
        new Date(data.createdat).getTime(),
        new Date().getTime(),
      )} days ago`;
    }

    const toggleOverlay = () => {
      this.setState({ isVisible: false });
    };

    return (
      <Container>
        <ParallaxScrollView
          parallaxHeaderHeight={200}
          backgroundImageSource={{
            uri: data?.introImg,
          }}
          renderStickyHeader={() => <Header backgroundColor="transparent" />}
        >
          <Body>
            <Image
              source={{
                uri: data?.user?.profile?.profileUrl,
              }}
              width={100}
              height={100}
              center
              resizeMode="contain"
              containerStyle={{ marginTop: 20 }}
            />
            <Text
              marginTop={10}
              fontSize={30}
              fontWeight="medium"
              fontFamily="GothamRoundedBold"
              color="#000000"
              center
              style={{ opacity: 0.8 }}
            >
              {data.user?.profile?.name}
            </Text>

            <QuickView
              justifyContent="center"
              alignItems="center"
              marginTop={20}
            >
              <Text
                fontSize={20}
                color="#188ded"
                fontWeight="bold"
                fontFamily="GothamRoundedBold"
              >
                {`$${data.lowestWage}`}
                -
                {`$${data.highestWage}`}
              </Text>
            </QuickView>

            <QuickView row justifyContent="space-between" margin={20}>
              <QuickView row flex={12} alignItems="center">
                <Text color="#a09a9a" fontSize={15}>
                  {datediff}
                </Text>
              </QuickView>
              <QuickView row flex={6} alignItems="center">
                <Text color="#707070" fontSize={15}>
                  32+ Applycants
                </Text>
              </QuickView>
            </QuickView>
            <QuickView row>
              <QuickView flex={2}>
                <TouchableOpacity
                  style={{
                    backgroundColor: '#dee1e3',
                    paddingHorizontal: 20,
                    paddingVertical: 15,
                    borderRadius: 5,
                  }}
                >
                  <Icon name="favorite" type="fontisto" color="#acb8bf" />
                </TouchableOpacity>
              </QuickView>
              <QuickView flex={10} paddingLeft={10}>
                { isApplied ? (
                  <TouchableOpacity
                    style={{
                      backgroundColor: '#a298e3',
                      alignItems: 'center',
                      justifyContent: 'center',
                      paddingVertical: 15,
                      borderRadius: 5,
                    }}
                    disabled
                  >
                    <Text color="#ffffff" fontWeight="medium" fontSize={18}>
                      Pending
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    style={{
                      backgroundColor: '#6e5ce6',
                      alignItems: 'center',
                      justifyContent: 'center',
                      paddingVertical: 15,
                      borderRadius: 5,
                    }}
                    onPress={async () => {
                      console.log('applied');
                      if (!token) {
                        NavigationService.navigate(rootStack.authStack);
                      } else {
                        NavigationService.navigate(exploreStack.applyScreen, {
                          id: data.id,
                        });
                      }
                    }}
                  >
                    <Text color="#ffffff" fontWeight="medium" fontSize={18}>
                      Apply Now
                    </Text>
                  </TouchableOpacity>
                )}

                {/* <Overlay
                  isVisible={isVisible}
                  onBackdropPress={toggleOverlay}
                  overlayStyle={{ borderRadius: 10 }}
                >
                  <QuickView
                    width={300}
                    height={150}
                    backgroundColor="#fff"
                    // center
                  >
                    <QuickView flex={1} center>
                      <Text color="#e32d14" fontSize={22} fontWeight="bold">
                        Warning
                      </Text>
                    </QuickView>
                    <QuickView center flex={1}>
                      <Text color="#8a8786" style={{ opacity: 0.7 }}>
                        {errMsg}
                      </Text>
                    </QuickView>
                    <QuickView flex={2}>
                      <Button title="Dismiss" center sharp color="#e32d14" />
                    </QuickView>
                  </QuickView>
                </Overlay> */}
              </QuickView>
            </QuickView>
            <TopTabs />
          </Body>
        </ParallaxScrollView>
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => ({
  detail: parseObjectSelector(applyObjectSelector(jobDetailSelector, state)),
  requireLogin: requireLoginSelector(state),
  loginSelectorData: applyObjectSelector(loginSelector, state),
  profile: parseObjectSelector(applyObjectSelector(profileSelector, state)),
});

const mapDispatchToProps = (dispatch: any) => ({
  getDetail: (id: string) => dispatch(jobGetDetail({ id })),
  getDetailProfile: (query?: TQuery) => dispatch(profileGetDetail({ query })),
  getList: () => dispatch(myJobsGetRecently({})),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withTheme(ApplicantScreens as any));
