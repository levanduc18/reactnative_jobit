import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  Body, Button, Container, Header, QuickView, Text,
} from '@components';
import { applyObjectSelector, parseObjectSelector } from '@utils/selector';
import { profileSelector } from '@contents/Main/containers/Profile/redux/selector';
import { getIdFromParams, Global } from '@utils/appHelper';
import DocumentPicker from 'react-native-document-picker';
import axios from 'axios';
import { StatusBar, TouchableOpacity, Alert } from 'react-native';
import NavigationService from '@utils/navigation';
import { profileGetDetail } from '@contents/Main/containers/Profile/redux/slice';
import { TQuery } from '@utils/redux';
import { myJobsGetApplied } from '@contents/Main/containers/MyJobs/redux/slice';
import { getDetailProfile } from '@contents/Main/containers/Profile/redux/saga';
import { Picker } from '@react-native-community/picker';
import { jobApplies } from '../../redux/slice';

interface Props {
  appliesJob: (id: string) => any;
  id: any;
  profile: any;
  getDetailProfile: any;
  getListApplied: any;
}

interface State {
  pickerValue: any
  pickItem: any
  isLoading: boolean
}
class ApplyScreen extends PureComponent<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = { pickerValue: '', pickItem: [], isLoading: true };
  }

  componentDidMount() {
    const { getDetailProfile } = this.props;
    getDetailProfile();
  }

  renderCenterComponent = () => (
    <Text color="#fff" fontSize={20} fontWeight="bold">
      Apply Job
    </Text>
  );

  render() {
    const {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      appliesJob,
      profile: { data },
      getListApplied,
    } = this.props;
    const self = this;
    axios.get(
      `https://7dc66fda7ef5.ap.ngrok.io/apply/cv/${data.id}`,
    ).then((response) => {
      self.setState({ pickItem: response.data.data });
    });
    return (
      <Container>
        <StatusBar backgroundColor="transparent" />
        <Header
          backIcon
          leftColor="#fff"
          backgroundColor="#5760eb"
          height={100}
          centerComponent={this.renderCenterComponent()}
        />
        <Body backgroundColor="#fff">
          <Text fontSize={18} center marginTop={40} color="#000">
            Following profile will be sent to the recruiter
          </Text>
          <QuickView
            marginTop={40}
            paddingHorizontal={20}
            paddingVertical={30}
            backgroundColor="#fff"
            style={{ flex: 10, borderRadius: 6 }}
          >
            <Text color="#000" bold fontSize={16} marginBottom={10}>
              Attached CV:
            </Text>
            <QuickView style={{ borderWidth: 1, borderColor: 'white', borderRadius: 4 }} backgroundColor="#f0f2f5" marginBottom={10}>
              <Picker
                style={{
                  width: '100%',
                  height: 40,
                  color: '#000',
                  fontSize: 14,
                  fontFamily: 'Roboto-Regular',
                }}
                selectedValue={this.state.pickerValue}
                onValueChange={(itemValue, itemIndex) => this.setState({ pickerValue: itemValue })}
              >
                {this.state.pickItem.map((item: { name: string, id: string }) => (
                  <Picker.Item label={item.name} value={item.id} />))}
              </Picker>
            </QuickView>
          </QuickView>
          <QuickView
            center
            row
            paddingHorizontal={20}
            paddingVertical={30}
            marginTop={20}
            style={{ flex: 5 }}
          >
            <Button
              width={100}
              backgroundColor="#5760eb"
              title="Upload"
              marginBottom={30}
              onPress={async () => {
                try {
                  const uploadFile = await DocumentPicker.pick({
                    type: [
                      DocumentPicker.types.pdf,
                      DocumentPicker.types.docx,
                      DocumentPicker.types.zip,
                    ],
                  });
                  const image: any = {
                    uri: uploadFile.uri,
                    type: uploadFile.type,
                    name: uploadFile.name,
                  };
                  const file = new FormData();
                  file.append('file', image);
                  const result: any = await axios.post(
                    'https://7dc66fda7ef5.ap.ngrok.io/upload',
                    file,
                    {
                      headers: {
                        'Content-Type': 'multipart/form-data; ',
                      },
                    },
                  );
                  await axios.post(
                    `https://7dc66fda7ef5.ap.ngrok.io/apply/cv/${data.id}`,
                    {
                      name: image.name,
                      cvURL: result.data.data.url,
                    },
                  );
                  if (result.data.data.url) {
                    this.setState({ isLoading: !this.state.isLoading });
                    Alert.alert('Upload successful', 'Your resume is updated');
                  }
                } catch (err) {
                  // eslint-disable-next-line no-console
                  console.log('err', err);
                  if (DocumentPicker.isCancel(err)) {
                    // User cancelled the picker, exit any dialogs or menus and move on
                  } else {
                    throw err;
                  }
                }
              }}
            />
            <Button
              backgroundColor="#5760eb"
              title="Submit"
              marginBottom={30}
              paddingLeft={20}
              width={100}
              onPress={async () => {
                if (this.state.pickerValue !== '') {
                  await axios.post(
                    `https://7dc66fda7ef5.ap.ngrok.io/apply/${getIdFromParams(
                      this.props,
                    )}`,
                    {
                      cvId: this.state.pickerValue,
                    },
                  );
                  getListApplied();
                  NavigationService.goBack();
                  Alert.alert('Sent resume', 'Thank you for submitting your resume');
                } else {
                  Alert.alert('No attached CV?', 'Please upload your CV');
                }
              }}
            />
          </QuickView>
        </Body>
      </Container>

    );
  }
}

const mapStateToProps = (state: any) => ({
  profile: parseObjectSelector(applyObjectSelector(profileSelector, state)),
});

const mapDispatchToProps = (dispatch: any) => ({
  appliesJob: (id: string) => dispatch(jobApplies({ id })),
  getDetailProfile: (query?: TQuery) => dispatch(profileGetDetail({ query })),
  getListApplied: () => dispatch(myJobsGetApplied({})),
});

export default connect(mapStateToProps, mapDispatchToProps)(ApplyScreen as any);
