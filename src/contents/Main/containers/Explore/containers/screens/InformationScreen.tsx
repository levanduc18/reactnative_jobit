/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  Container, QuickView, Text, FlatList,
} from '@components';
import { Avatar, Divider, withTheme } from 'react-native-elements';
import HTML from 'react-native-render-html';
import { applyObjectSelector, parseObjectSelector } from '@utils/selector';
import { requireLoginSelector } from '@contents/Config/redux/selector';
import { loginSelector } from '@contents/Auth/containers/Index/Login/redux/selector';
import { ActivityIndicator } from 'react-native';
import Moment from 'moment';
import { jobDetailSelector } from '../../redux/selector';

interface Props {
  detail?: any;
  requireLogin?: boolean;
  loginSelectorData?: any;
}

class InformationScreen extends PureComponent<Props> {
  renderListTags = ({ item }: { item: any }) => (
    <QuickView
      backgroundColor="#E3F8F9"
      marginLeft={10}
      marginTop={10}
      borderRadius={5}
      paddingHorizontal={10}
      paddingVertical={2}
    >
      <Text
        color="#04B9D0"
        fontWeight="bold"
        fontSize={14}
        fontFamily="GothamRoundedBold"
      >
        {item}
      </Text>
    </QuickView>
  );

  render() {
    const {
      detail: { data, loading },
    } = this.props;

    return (
      <Container>
        <QuickView>
          <QuickView marginTop={10}>
            <QuickView marginTop={30}>
              <QuickView row>
                <QuickView backgroundColor="#B5BABD" borderRadius={5}>
                  <Text
                    color="#fff"
                    fontSize={12}
                    fontWeight="bold"
                    style={{
                      paddingHorizontal: 10,
                    }}
                  >
                    Full Time
                  </Text>
                </QuickView>
              </QuickView>
              <QuickView alignItems="center" paddingRight={30}>
                <Text
                  fontSize={28}
                  color="#1D1D1D"
                  fontWeight="bold"
                  fontFamily="GothamRoundedBold"
                >
                  {/* {data.data?.name} */}
                </Text>
              </QuickView>
              <QuickView row>
                <Text
                  color="#B5BABD"
                  fontFamily="GothamRoundedBold"
                  style={{ opacity: 0.5 }}
                >
                  Posted on
                </Text>
                <Text color="#B5BABD" marginLeft={5}>
                  {/* {Moment(data.data?.createdat).format('D/M/Y')} */}
                </Text>
              </QuickView>
            </QuickView>
            <Divider
              style={{ backgroundColor: '#d9d9d9', height: 2, marginTop: 20 }}
            />
            <QuickView row alignItems="center" marginTop={10}>
              <QuickView>
                {/* <Avatar source={{ uri: detailItem.logo }} rounded /> */}
              </QuickView>
              <QuickView marginLeft={10}>
                <Text
                  color="1D1D1D"
                  fontWeight="bold"
                  fontFamily="GothamRoundedBold"
                  fontSize={14}
                >
                  COMPANY
                </Text>
                <Text color="#B5BABD" fontSize={14}>
                  {/* {data.data?.user.profile.name} */}
                </Text>
              </QuickView>
              <QuickView marginLeft={10}>
                <Text
                  color="1D1D1D"
                  fontWeight="bold"
                  fontFamily="GothamRoundedBold"
                  fontSize={14}
                >
                  LOCATION
                </Text>
                <Text color="#B5BABD" fontSize={14} numberOfLines={2}>
                  {/* {data.data?.address.description} */}
                </Text>
              </QuickView>
            </QuickView>

            <QuickView>
              {/* <FlatList
                data={detailItem.tag}
                renderItem={this.renderListTags}
                contentContainerStyle={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                }}
              /> */}
            </QuickView>
            {loading ? (
              <QuickView>
                <ActivityIndicator size="large" color="#ff6a00" />
              </QuickView>
            ) : (
              <QuickView marginTop={10}>
                <HTML html={data.data?.description} />
                <HTML html={data.data?.content} />
              </QuickView>
            )}
          </QuickView>
        </QuickView>
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => ({
  detail: parseObjectSelector(applyObjectSelector(jobDetailSelector, state)),
  requireLogin: requireLoginSelector(state),
  loginSelectorData: applyObjectSelector(loginSelector, state),
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InformationScreen as any);
